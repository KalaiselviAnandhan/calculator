import React from "react"

function InputPanel(props){
    
    const handle = (event)=> props.onClick(event.target.name)
    let inputs = ["(",")","CE","C","1","2","3","+","4","5","6","-","7","8","9","X",".","0","=","/"]
    let buttons = inputs.map((item,i)=>item !== 'X' ? <button name = {item} key={i} onClick={handle}>{item}</button> : <button name = "*"  key={i} onClick={handle}>{item}</button> )

    return(
        <div className = "button">
            {buttons}
        </div>
    )
}

export default InputPanel