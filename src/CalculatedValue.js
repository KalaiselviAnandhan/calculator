import React from "react"

function CalculatedValue(props){
    return(
    <div className = "outputPanel">
        <p>{props.history}</p>
        <p>{props.result}</p>
    </div>
    )
}

export default CalculatedValue