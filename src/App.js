import React from 'react'
import './App.css'
import InputPanel from './InputPanel'
import {evaluate} from 'mathjs'
import CalculatedValue from './CalculatedValue'
import ShowAllHistory from './ShowAllHistory'
import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";
import KeyPad from './KeyPad'


class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      result : ''
    }
  }

  expression = ''

  onClick = val => ((val === '=') ? this.CalculatedValue() : ((val === 'C') ? this.backSpace()  : ((val === 'CE') ? this.reSet() : this.setState({result : this.state.result + val}) )))

  CalculatedValue = () => {
    if(/[0-9]$/.test(this.state.result)){
      this.expression = this.state.result
      this.setState({
        result : evaluate(this.expression),
      },()=>{
        this.saveToLocal()
      })
    }
    else{
      alert("Please enter a number to continue...")
    }
  }

  handler = (val,key)=>{
    if(key === 'Enter'){
      if(!(/[a-z@!=|#$%^&_:;{},`~?]/.test(val))){
        this.setState({
          result:val
        },() => this.CalculatedValue())
      }
      else{
        alert("Please enter valid expression")
      }
    }
    if(key === 'Backspace'){
      this.reSet()
    }
  }

  saveToLocal = () => {
    let obj={}
    if(this.state.result !== ''){
      const localHistory = JSON.parse(localStorage.getItem("History")) || []
      obj["result"] = this.state.result
      obj["expression"] = this.expression
      localHistory.push(obj)
      localStorage.setItem("History",JSON.stringify(localHistory))
    }
  }

  reSet = () => {
    this.setState({
      result : ''
    })
    this.expression = ''
  }

  backSpace = () => {
    if(typeof this.state.result !== 'number'){
      this.setState({
        result : this.state.result.slice(0,-1)
      })
    }
  }

  render(){

    return(

      <Router>
        <div>
          <ul>
            <li>
              <h1>Calculator!</h1>
            </li>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/history">History</Link>
            </li>
          </ul>

          <Switch>
            <Route exact path="/">
              <div className = "app">
                <KeyPad onKeyDown = {this.handler}/>
                <CalculatedValue result = {this.state.result} history = {this.expression} />
                <InputPanel onClick = {this.onClick} />
              </div>
            </Route>
            <Route path="/about">
              <div className="about">
                <h1>Hi, I am Kalaiselvi Anandhan</h1>
                <p>a web developer</p>
              </div>
            </Route>
            <Route path="/history">
              <ShowAllHistory  calcHistory = {JSON.parse(localStorage.getItem("History") !== null ? localStorage.getItem("History") : JSON.stringify([]))} />
            </Route>
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App;
