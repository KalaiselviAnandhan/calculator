import React from "react"


function KeyPad(props){
    const handle = ((event)=> {
        props.onKeyDown(event.target.value,event.key)
    })
    return(
        <div>
            <input type="text" name = "expression"  placeholder="Enter the expression.." onKeyDown = {handle} />
        </div>
    )
}

export  default KeyPad