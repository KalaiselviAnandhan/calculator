import React from "react"

class ShowAllHistory extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            isActive : false
        }
    }

    handleClick = (event)=>{
        this.setState({
            isActive:!this.state.isActive
        })
    }

    render(){
        
        const all = this.props.calcHistory.map((item,i)=>(item !== '')?<div key = {i}><p>{`${item.expression} = ${item.result}`}</p></div>:null)
        return(
            <div className="AllContent" >
                <button onClick = {this.handleClick}>HISTORY</button>
                <div className = 'container' style = {{display : this.state.isActive ? 'block':'none'}}>{all}</div>
            </div>
        )
    }
}

export default ShowAllHistory